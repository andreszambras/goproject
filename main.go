package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type Article struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Content     string `json:"content"`
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: homePage")
	fmt.Fprintf(w, "Welcome to the HomePage!")
}

func returnAllArticles(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllArticles")

	db, err := dbConnection()
	if err != nil {
		panic(err.Error())
	}

	res, err := db.Query("SELECT * FROM article")
	defer res.Close()

	if err != nil {
		panic(err.Error())
	}

	var articles []Article
	for res.Next() {
		var article Article
		// for each row, scan the result into our tag composite object
		err = res.Scan(&article.Id, &article.Title, &article.Description, &article.Content)
		if err != nil {
			panic(err.Error())
		}

		articles = append(articles, article)
	}

	json.NewEncoder(w).Encode(articles)
}

func returnSingleArticle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnSingleArticle")

	vars := mux.Vars(r)
	id := vars["id"]

	db, err := dbConnection()
	if err != nil {
		panic(err.Error())
	}

	res, err := db.Query("SELECT * FROM article where id = ?", id)
	defer res.Close()

	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	var article Article
	if res.Next() {

		// for each row, scan the result into our tag composite object
		err = res.Scan(&article.Id, &article.Title, &article.Description, &article.Content)
		if err != nil {
			panic(err.Error())
		}

	}

	json.NewEncoder(w).Encode(article)
}

func createNewArticle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: createNewArticle")

	reqBody, _ := ioutil.ReadAll(r.Body)
	var article Article
	json.Unmarshal(reqBody, &article)

	db, err := dbConnection()
	if err != nil {
		panic(err.Error())
	}

	res, err := db.Query("INSERT INTO article (title, description, content) VALUES (?, ?, ?)", article.Title, article.Description, article.Content)
	defer res.Close()

	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Successfully Created!")
}

func modifyArticle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: createNewArticle")

	vars := mux.Vars(r)
	id := vars["id"]

	reqBody, _ := ioutil.ReadAll(r.Body)
	var article Article
	json.Unmarshal(reqBody, &article)

	db, err := dbConnection()
	if err != nil {
		panic(err.Error())
	}

	res, err := db.Query("UPDATE article SET title = ?, description = ?,  content = ? WHERE id = ?", article.Title, article.Description, article.Content, id)
	defer res.Close()

	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Successfully modified!")
}

func deleteArticle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: deleteArticle")

	vars := mux.Vars(r)
	id := vars["id"]

	db, err := dbConnection()
	if err != nil {
		panic(err.Error())
	}

	res, err := db.Query("DELETE  FROM article WHERE id = ?", id)
	defer res.Close()

	if err != nil {
		panic(err.Error())
	}

	fmt.Fprintf(w, "Successfully removed!")
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/articles", returnAllArticles)
	myRouter.HandleFunc("/article", createNewArticle).Methods("POST")
	myRouter.HandleFunc("/article/{id}", modifyArticle).Methods("PUT")
	myRouter.HandleFunc("/article/{id}", deleteArticle).Methods("DELETE")
	myRouter.HandleFunc("/article/{id}", returnSingleArticle)
	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func dbConnection() (*sql.DB, error) {
	fmt.Println("Connection")

	db, err := sql.Open("mysql", "root:@tcp(localhost:3306)/test")

	if err != nil {
		return nil, err
	}

	return db, nil
}

func main() {
	handleRequests()
}
