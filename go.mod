module GoProject

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.2
)